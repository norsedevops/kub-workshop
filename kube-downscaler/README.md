# Repos du guerrier

Vu qu'on a bien travaillé, on peut se reposer un peu et nos pods aussi, on va donc mettre en place [kube-downscaler](https://codeberg.org/hjacobs/kube-downscaler.git) pour éteindre nos workloads lors des temps de pause.

## Installation

`kube-downscaler` est un projet plus récent, il n'est pas encore très industrialisé donc il est nécessaire de faire quelques adaptations pour le déployer:

- changer le namespace `default` en `kube-downscaler` dans `deploy/rbac.yml`
- désactiver le mode `dry-run` dans `deploy/deployment.yml` 
- (pour la démo) réduire la grace period de prise en compte des composants à 0min (plutôt que 15 par défaut) dans `deploy/deployment.yml`
- (pour la démo) appliquer uniquement les policies sur le namespace `demos` dans `deploy/deployment.yml`

```yaml
    - --grace-period=0
    - --namespace=demos
```

??? example "Pour les curieux, les fichiers `rbac.yml` et `deployment.yml`"
    `rbac.yml`:
    ```yaml
    --8<-- "kube-downscaler/deploy/rbac.yaml"
    ```

    `deployment.yml`:
    ```yaml
    --8<-- "kube-downscaler/deploy/deployment.yaml"
    ```

Les fichiers sont dans le répertoire `kube-downscaler/deploy`, on utilise [kustomize](https://kustomize.io/) pour déployer (contrairement à Helm les autres fois).

```bash
kubectl apply -k kube-downscaler/deploy/ # (1)
```

1. On utilise `-k` au lieu du `-f` habituel

!!! success "Tout est créé"
    ```console
    namespace/kube-downscaler created
    serviceaccount/kube-downscaler created
    clusterrole.rbac.authorization.k8s.io/kube-downscaler created
    clusterrolebinding.rbac.authorization.k8s.io/kube-downscaler created
    configmap/kube-downscaler created
    deployment.apps/kube-downscaler created    
    ```

On vérifie que tout est ok

```bash
kubectl get all -n kube-downscaler
```

!!! success "Tout est opérationnel"
    ```console
    NAME                                  READY   STATUS    RESTARTS   AGE
    pod/kube-downscaler-f5cbb6cfc-f2j9w   1/1     Running   0          39s

    NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/kube-downscaler   1/1     1            1           39s

    NAME                                        DESIRED   CURRENT   READY   AGE
    replicaset.apps/kube-downscaler-f5cbb6cfc   1         1         1       39s    
    ```

## Test

Vu qu'on aime pas trop bosser, on va configurer `kube-downscaler` pour que nos pods ne soient démarrés que le lundi de 7h30 à 18h 🫢

```yaml
  annotations:
    downscaler/uptime: Mon-Mon 07:30-18:00 CET
    downscaler/force-downtime: "true"
```

Pour cela, on annote notre namespace `demos` pour qu'il ne soit up que sur cette période.

*NB:* `kube-dowscaler` peut être configuré à différents niveaux, on aurait pu annoter les deployments directement plutôt que le namespace.

```bash
kubectl annotate ns demos 'downscaler/uptime=Mon-Mon 07:30-18:00 CET'
```

!!! success "Le namespace est annoté pour s'éteindre"
    ```bash
    kubectl describe ns demos
    ```
    ```console
    Name:         demos
    Labels:       kubernetes.io/metadata.name=demos
    Annotations:  downscaler/uptime: Mon-Fri 21:30-23:30 CET
    Status:       Active

    No resource quota.

    No LimitRange resource.    
    ```

On peut voir immédiatement que nos pods sont éteints et que nos déploiements sont à 0

!!! success "Tout est éteint"
    ```bash
    kubectl get po -n demos
    ```
    ```console
    No resources found in demos namespace
    ```

    et les déploiements
    ```bash
    kubectl get deploy -n demos
    ```
    ```console
    NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
    asciinematic-hardened   0/0     0            0           156m
    new-deployment          0/0     0            0           28h
    simple-deployment       0/0     0            0           28h    
    ```

On peut voir dans les logs de `kube-downscaler` qu'il a traité notre configuration

```bash
kubectl logs -f $(kubectl get po -n kube-downscaler | grep kube-downscaler | cut -d' ' -f1) -n kube-downscaler
```

!!! info
    ```console
    2023-12-20 18:28:55,501 INFO: Downscaler v23.2.0 started with debug=False, default_downtime=never, default_uptime=Mon-Fri 07:30-20:30 CET, deployment_time_annotation=None, downscale_period=never, downtime_replicas=0, dry_run=False, enable_events=False, exclude_deployments=kube-downscaler,downscaler, exclude_namespaces=kube-system, grace_period=0, include_resources=deployments, interval=2, namespace=demos, once=False, upscale_period=never
    2023-12-20 18:47:29,649 INFO: Scaling down Deployment demos/asciinematic-hardened from 1 to 0 replicas (uptime: Mon-Fri 21:30-23:30 CET, downtime: never)
    2023-12-20 18:47:29,675 INFO: Scaling down Deployment demos/new-deployment from 1 to 0 replicas (uptime: Mon-Fri 21:30-23:30 CET, downtime: never)
    2023-12-20 18:47:29,700 INFO: Scaling down Deployment demos/simple-deployment from 1 to 0 replicas (uptime: Mon-Fri 21:30-23:30 CET, downtime: never)
    ```

**Voilà, on a une recette bien complète, faisons un petit récapitulatif [➡️](../recipe.md)**