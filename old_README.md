# A cool Kubernetes workshop

1️⃣ Set up cluster with Terraform: [README.md](terraform/README.md)

2️⃣ Set up an Ingress controller with Nginx : [README.md](nginx-ingress-controller/README.md)

3️⃣ Set up External-DNS: [README.md](external-dns/README.md)

4️⃣ Set up Cert-manager: [README.md](cert-manager/README.md)

5️⃣ Set up External-secrets: [README.md](external-secrets/README.md)

6️⃣ Set up Kyverno: [README.md](kyverno/README.md)

7️⃣ Set up Kube-downscaler: [README.md](kube-downscaler/README.md)

8️⃣ Set up GitLab: [README.md](gitlab/README.md)

9️⃣ Set up Flux: [README.md](flux/README.md)


Before leaving don't forget to remove ingress (due to External DNS, records exist in your DNS) and destroy your cluster ;-)
```bash
for i in $(kubectl get ingress -A --no-headers -o=name); do
  kubectl delete $i -n demos
done
```
